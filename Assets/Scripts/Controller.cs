﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {
  [SerializeField]
  private Camera _camera;
	public int speed = 0;
	
// Use this for initialization
   void Start () {

  }
// Update is called once per frame
   void Update () {
         _camera.transform.position = transform.position + transform.forward * 1.0f;

// get input data from keyboard or controller
     float moveHorizontal = Input.GetAxis("Horizontal");
     float moveVertical = Input.GetAxis("Vertical");
// update player position based on input
      Vector3 position = transform.position;
      position.x += moveHorizontal * speed * Time.deltaTime;
      position.z += moveVertical * speed * Time.deltaTime;
      transform.position = position;
      }
  }